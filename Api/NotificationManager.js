import React from 'react';
import PushNotification from 'react-native-push-notification';
import {Platform} from 'react-native'
import { Data } from '../Config';
export const ConfigNotify = () => {
    console.log('11111111111111111111')
    PushNotification.configure({
        onRegister: function (token) {
            console.log(token);
        },
        onNotification: function(notification) {
            console.log('Notification ', notification);
        },
        popInitialNotification: true,
        requestPermissions: true,
        requestPermissions: Platform.OS === 'ios',
    })
}

const _localNotify = (id, title, message, data = {}, option = {}) => {
    return{
        id: id,
        autoCancel: true,
        largeIcon: option.largeIcon || 'ic_lancher',
        smallIcon: option.smallIcon || 'ic_lancher',
        bigText: message || '',
        subText: title || '',
        vibration: option.vibration || false,
        priority: option.priority || 'high',
        importance: option.importance || 'high',
        data: data
    }
}

export const createChanel = (val) => {
    PushNotification.createChannel({
        channelId: val,
        channelName: 'Name',
        channelDescription: 'dfkghdfhgl',
        playSound: false,
        soundName: 'default',
        importance: 4,
        vibrate: true
    },
    (created) => {console.log(created)}
    )
}


export const showNotification = async (id, title, message, data = {}, option = {}) => {

    let val = await _localNotify(id, title, message, data, option);
    console.log('jgfjdbh')
    PushNotification.localNotificationSchedule({
        channelId: id,
        title: title || '',
        date: new Date(Date.now() + (1 * 100)) ,
        message: message || '',
        playSound: option.playSound || false,
        soundName: option.soundName || 'default',
        userInteraction: false,
        largeIcon: option.largeIcon || 'ic_lancher',
        smallIcon: option.smallIcon || 'ic_lancher',
        largeIconUrl: 'https://img.icons8.com/ios/452/cloud.png',
        ongoing:true
    })
}


export const cancelAllNotification = () => {
    PushNotification.cancelAllLocalNotifications()
}

export const unregister = () => {
    PushNotification.unregister()
}