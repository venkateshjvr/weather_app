import React from 'react';
import {ToastAndroid} from 'react-native';
import axios from 'axios';
import { Data } from '../Config';
import {createChanel, ConfigNotify, showNotification } from '../Api/NotificationManager';

export const listCity = async (setLoader, setCity, position, notify) => {
    try {
        let ur = `http://api.openweathermap.org/data/2.5/find?lat=9.9163256&lon=78.124392&cnt=50&appid=${Data.apiKey}`
        if (position && position.coords) {
            ur = `http://api.openweathermap.org/data/2.5/find?lat=${position.coords.latitude}&lon=${position.coords.longitude}&cnt=50&appid=${Data.apiKey}`
        }
        let dt = await axios({
            url : ur,
            method: 'GET'
        });
        if(dt.data && dt.data.cod == 200) {
            setCity(dt.data.list);
            setLoader(false);
            if(notify && notify == 1) {
                if (dt.data.list.length != 0) {
                    ConfigNotify();
                    createChanel('1')
                    showNotification(
                        '1',
                        "Weather App",
                        `Current Temparature : ${(dt.data.list[0].main.temp - 273.15).toFixed(2)}\u00B0C`,
                        {},
                        {}
                    )
                }
            }
        } else {
            ToastAndroid.show('Something Went Worng!', ToastAndroid.LONG)
            setLoader(false);
        }
    } catch (error) {
        console.log(error);
        setLoader(false);
        ToastAndroid.show('Network Error!', ToastAndroid.LONG)
    }
}