import React, { useState, useEffect } from 'react';
import {View, Text, StatusBar, FlatList, RefreshControl, TouchableOpacity, PermissionsAndroid} from 'react-native';
import { Data } from '../../Config';
import { styles } from './style';
import LottieView from 'lottie-react-native';
import { listCity } from '../../Api/listCity';
import PushNotification from 'react-native-push-notification';
import GeoLocation from '@react-native-community/geolocation';
import {createChanel, ConfigNotify, showNotification } from '../../Api/NotificationManager';
export default function Home ({navigation}) {
    const [loader, setLoader] = useState(true);
    const [city, setCity] = useState([]);
    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        weatherArray(setRefreshing, 0);
    }, []);

    let weatherArray = async (setLoader1, notify) =>{
        try {
            const grand = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
                title: "Infinity",
                message:
                "Need to access Location",
                buttonNeutral: "Ask Me Later",
                buttonNegative: "Cancel",
                buttonPositive: "OK"

            })
            if(grand === PermissionsAndroid.RESULTS.GRANTED){
                console.log(grand);
                GeoLocation.getCurrentPosition(position => {
                    listCity(setLoader1, setCity, position, notify);
                    console.log(position);
                })
            } else {
                listCity(setLoader1, setCity);
                console.log('No Access');
            }
        } catch (error) {
            listCity(setLoader1, setCity);
            console.log('---------------',error)
        }
    }
    useEffect(()=>{
        setLoader(true)
        weatherArray(setLoader, 1)
    },[])
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={Data.baseColor} />
            <View style={styles.header}>
                <Text style={styles.headerText}>Weather App</Text>
            </View>
            {loader && <View style={styles.loaderContainer}>
                <LottieView source={require('../../assets/32532-day-night.json')} autoPlay loop style={{height: 150, width: 150}} />
            </View>}
            {!loader && <View style={styles.viewContainer}>
                <FlatList
                    data={city}
                    renderItem={({item}) =>{
                    return (
                        <TouchableOpacity style={styles.buttonContainer}
                            onPress={()=>{
                                navigation.navigate('MapLocation',{item: item})
                            }}
                        >
                        <View style={styles.descriptionContainer}>
                            <Text style={styles.title}>{item.name}</Text>
                            <Text style={styles.subTitle}>{item.weather[0].description}</Text>
                        </View>
                        <View style={{height : '100%', width : '30%', justifyContent: 'center'}}>
                            <Text style={{fontSize: 20}}>{(item.main.temp - 273.15).toFixed(2)}&deg; <Text style={{fontSize: 16}}>C</Text> </Text>
                        </View>
                        </TouchableOpacity>)}
                    }
                    keyExtractor={(item) => item.id}
                    refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} colors={['red', 'green']} />}
                />
                </View>
            }
        </View>
    )
}