import React, { useState } from 'react';
import {StatusBar, View, Text, Dimensions} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
let {height, width} = Dimensions.get('window');
import { styles } from './style';
import { Data } from '../../Config';
import { Icon } from 'react-native-elements';
export default function MapLocation ({route, navigation}) {
    const [obj, setObj] = useState(route.params.item)
    return (
        <View style={styles.container}>
            <MapView 
                style={styles.mpView}
                provider={PROVIDER_GOOGLE}
                initialRegion={{
                    latitude: obj.coord.lat,
                    longitude: obj.coord.lon,
                    latitudeDelta: 0.0600,
                    longitudeDelta: 0.0421
                }}
            >
                <Marker coordinate={{latitude: obj.coord.lat, longitude: obj.coord.lon }}>
                    <Icon name='location-pin' type='entypo' color='red' size={35} />
                </Marker>
            </MapView>
            <StatusBar backgroundColor={Data.baseColor} />
            <View style={styles.header}>
                <View style={styles.headerIconContainer}>
                    <Icon name='arrowleft' type='antdesign' color='#fff' size={25} onPress={()=>{
                        navigation.goBack(null);
                    }} />
                </View>
                <Text style={styles.headerText}>Weather App</Text>
            </View>
            <View style={styles.contentContainer}>
                <View style={styles.content1}>
                    <Text style={styles.textHeading}>{obj.name}</Text>
                    <Text style={styles.text1}>{obj.weather[0].description}</Text>
                    <Text style={styles.text2}><Text>Humidity : </Text> {obj.main.humidity}</Text>
                    <Text style={styles.text2}><Text>Wind Speed : </Text> {obj.wind.speed}</Text>
                    <Text style={styles.text2}><Text>Max. Temp : </Text> {(obj.main.temp_max - 273.15).toFixed(2)}&deg; <Text style={{fontSize: 13}}>C</Text></Text>
                    <Text style={styles.text2}><Text>Min Temp : </Text> {(obj.main.temp_min - 273.15).toFixed(2)}&deg; <Text style={{fontSize: 13}}>C</Text></Text>
                </View>
                <View style={styles.content2}>
                    <Text style={styles.text3}>{(obj.main.temp - 273.15).toFixed(2)}&deg; <Text style={{fontSize: 16}}>C</Text> </Text>
                    <Icon name='cloudy-outline' type='ionicon' size={65} color='#108' />
                </View>
            </View>
        </View>
    )
}
